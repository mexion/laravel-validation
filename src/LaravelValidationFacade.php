<?php

namespace Mexion\LaravelValidation;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Mexion\LaravelValidation\LaravelValidation
 */
class LaravelValidationFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return LaravelValidation::class;
    }
}
