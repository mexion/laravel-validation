<?php

namespace Mexion\LaravelValidation\Rules\Exceptions;

use InvalidArgumentException;

class RuleSetupException extends InvalidArgumentException
{
}
